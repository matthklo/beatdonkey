
--[[
    =======================================================
    Logging 
    =======================================================
  ]]

-- Set to false to turn off all logs.
enable_logging = true;

function log(msg)
    if enable_logging then
        reaper.ShowConsoleMsg(msg);
    end
end

--[[
    =======================================================
    A tiny IMGUI framework. 
    =======================================================
  ]]

-- global gui state
--   event type: 0: repaint, 1: mouse-down, 2: mouse-up, 3: key-down
--   key_code: the key code for the last key-down event
gui_state = { ['event_type'] = 0, ['key_code'] = 0, ['mouse_down'] = false, ['skin'] = { ['fgcolor'] = {1,1,1,1}, ['bgcolor'] = {0,0,0,1} } }

-- Input: A point (x,y) and a rect ({x,y,w,h}). 
-- Return: A boolean indicating whether the point locates inside the rect.
function is_in_rect(x, y, rect)
    if (x < rect[1]) or (x > (rect[1] + rect[3])) then
        return false;
    elseif (y < rect[2]) or (y > (rect[2] + rect[4])) then
        return false;
    end
    return true;
end

-- Input: A color (An array of at least 3 number elements. Each element ranges from 0 to 1.)
function set_color(c)
    gfx.r = tonumber(c[1]);
    gfx.g = tonumber(c[2]);
    gfx.b = tonumber(c[3]);
    if c[4] == nil then
        gfx.a = 1;
    else
        gfx.a = tonumber(c[4]);
    end
end

function set_color_fg()
    set_color(gui_state['skin']['fgcolor']);
end

function set_color_bg()
    set_color(gui_state['skin']['bgcolor']);
end

-- [IMGUI Widget] Button
-- Input: A rect ({x,y,w,h}) for the widget. A label (string) show on the button.
-- Return: A boolean value -- 'true' when clicked, 'false' otherwise.
function button(rect, label)

    if gui_state.event_type == 0 then  -- repaint

        local offset = 0;
        set_color_fg();

        -- Check if the mouse cursor locates inside the rect and the mouse button is clicking-down.
        -- Apply an offset on the rect to simulate the 'pressed-down' visual effect.
        if (gfx.mouse_cap & 1 == 1) and is_in_rect(gfx.mouse_x, gfx.mouse_y, rect) then
            offset = 3;
        end

        -- Draw button outline
        gfx.roundrect(rect[1] + offset, rect[2] + offset, rect[3], rect[4], 8);

        -- Draw button label (centered)
        local labelstr = tostring(label);
        local w, h = gfx.measurestr(labelstr);
        gfx.x = rect[1] + offset + ((rect[3] - w) // 2);
        gfx.y = rect[2] + offset + ((rect[4] - h) // 2);
        gfx.drawstr(labelstr);

    elseif gui_state.event_type == 2 then  -- mouse-up
        if is_in_rect(gfx.mouse_x, gfx.mouse_y, rect) then
            return true;
        end
    end

    return false;
end

-- [IMGUI Widget] Label
-- Input: A rect ({x,y,w,h}) for the widget. A label (string) to show.
-- Return: A boolean value -- 'true' when clicked, 'false' otherwise.
function label(rect, label)
    if gui_state.event_type == 0 then -- repaint
        -- draw button label (v-centered)
        set_color_fg();
        local labelstr = tostring(label);
        local w, h = gfx.measurestr(labelstr);
        gfx.x = rect[1];
        gfx.y = rect[2] + ((rect[4] - h) // 2);
        gfx.drawstr(labelstr);
    elseif gui_state.event_type == 2 then  -- mouse-up
        if is_in_rect(gfx.mouse_x, gfx.mouse_y, rect) then
            return true;
        end
    end
    return false;
end

-- [IMGUI Widget] Text Field (single line)
-- Input: A rect ({x,y,w,h}) for the widget. A state object (table, should be an empty table ({}) on the first call).
-- Return: Updated object ({text,cursoridx,cursordrew}).
function textfield(rect, state)
    if gui_state.event_type == 0 then -- repiant
        -- backgroud fill
        set_color_fg();
        gfx.rect(rect[1], rect[2], rect[3], rect[4]);

        -- draw text (v-centered)
        set_color_bg();
        if state.text ~= nil then
            local t = tostring(state.text);
            local w, h = gfx.measurestr(t);
            gfx.x = rect[1] + 2;
            gfx.y = rect[2] + ((rect[4] - h) // 2) + 2;
            gfx.drawstr(t);
        end

        -- draw cursor when needed
        if state.cursoridx ~= nil then
            -- Note: Only draw cursor on every other frame. To simulate a 'flashing' visual effect.
            if (state.cursordrew == nil) or (state.cursordrew == false) then
                state.cursordrew = true; -- skip drawing
            else
                state.cursordrew = false;
                local cidx = tonumber(state.cursoridx);

                if state.text ~= nil then
                    local t = tostring(state.text:sub(1, cidx));
                    local w, h = gfx.measurestr(t);
                    gfx.x = rect[1] + w + 2;
                    gfx.y = rect[2] + 2;
                    gfx.lineto(rect[1] + w + 2, rect[2] + rect[4] - 2);
                else
                    gfx.x = rect[1] + 2;
                    gfx.y = rect[2] + 2;
                    gfx.lineto(rect[1] + 2, rect[2] + rect[4] - 2);
                end
            end
        end
    elseif gui_state.event_type == 2 then
        -- mouse-up
        if is_in_rect(gfx.mouse_x, gfx.mouse_y, rect) and (state.cursoridx == nil) then
            if state.text == nil then
                state.cursoridx = 1
            else
                state.cursoridx = #state.text + 1;
            end
        end
    elseif gui_state.event_type == 3 then
        -- key-up
        if state.cursoridx ~= nil then
            local ch = gui_state.key_code;
            if (ch > 31) and (ch < 127) then
                state.cursoridx = state.cursoridx + 1;

                if state.text == nil then
                    state.text = string.char(ch);
                else
                    state.text = state.text .. string.char(ch);
                end
            elseif ch == 8 then  -- backspace
                if state.cursoridx > 1 then
                    state.cursoridx = state.cursoridx - 1;
                    state.text = state.text:sub(1, state.cursoridx - 1);
                else
                    state.cursoridx = 1;
                end
            elseif ch == 13 then -- enter
                state.cursoridx = nil;
            end
        end
    end

    return state;
end

-- [IMGUI Widget] Checkbox
-- Input: A rect ({x,y,w,h}) for the widget. A label string (optional). Current checked state.
-- Return: A boolean indicating the updated checked state.
function checkbox(rect, text, checked)
    if gui_state.event_type == 0 then -- repaint
        set_color_fg();
        local off = (rect[4] - 14) // 2;
        gfx.rect(rect[1] + off, rect[2] + off, 14, 14, false); -- outline of checkbox

        -- draw check
        if checked then
            local w, h = gfx.measurestr('v');
            gfx.x = rect[1] + off + ((14 - w)//2);
            gfx.y = rect[2] + off + ((14 - h)//2);
            gfx.drawstr('v');
        end

        -- draw text
        if text ~= nil then
            local t = tostring(text);
            local w2,h2 = gfx.measurestr(t);
            gfx.x = rect[1] + (off * 2) + 14;
            gfx.y = rect[2] + ((rect[4] - h2)//2);
            gfx.drawstr(t);
        end
    elseif gui_state.event_type == 2 then -- mouse-up
        if is_in_rect(gfx.mouse_x, gfx.mouse_y, rect) then
            checked = not checked;
        end
    end
    return checked;
end

--[[
    =======================================================
    Helper functions for persistent properties. 
    =======================================================
  ]]
function getprop(key)
    local retval, str = reaper.GetProjExtState(0, 'beatdonkey', key);
    -- retval is 1 when the key exist. otherwise, 0.
    -- log('retval: ' .. tostring(retval) .. ', str: ' .. tostring(str) .. '\n');
    if retval == 0 then
        return nil;
    end
    return str;
end

function setprop(key, val)
    reaper.SetProjExtState(0, 'beatdonkey', key, val);
end

function delprop(key)
    reaper.SetProjExtState(0, 'beatdonkey', key, "");
end

function delallprops(key)
    reaper.SetProjExtState(0, 'beatdonkey', "", "");
end

-- Main GUI layout function.
-- By the design of IMGUI, it also acts as an event responder.
function do_gui()

    -- Beat Count
    label({10, 10, 100, 20}, 'Beat Count: ' .. tostring(total_beats));

    -- Play / Stop control
    local is_playing = true;
    local playbtn_label = 'Stop';
    if 0 == reaper.GetPlayState() then
        is_playing = false;
        playbtn_label = 'Play';
    end    
    if button({gfx.w - 60, 10, 50, 20}, playbtn_label) then
        toggle_play();
    end

    -- Playing indicator
    if is_playing then
        local mi_start_sec = reaper.GetMediaItemInfo_Value(active_mi, 'D_POSITION');
        local mi_end_sec = mi_start_sec + reaper.GetMediaItemInfo_Value(active_mi, 'D_LENGTH');
        local cpos = reaper.GetPlayPosition();
        if (cpos >= mi_start_sec) and (cpos <= mi_end_sec) then
            local idx = math.floor((cpos - mi_start_sec) / beat_sec);
            gfx.r = 1;
            gfx.g = 0.2;
            gfx.b = 0.2;
            gfx.a = 1;
            gfx.rect(125 + (20 * idx), 35, 20, gfx.h - 70);
        end
    end

    -- Tracks
    local tidx = 1;
    while tracks[tidx] ~= nil do
        local y = 40 + ((tidx - 1) * 30);

        -- Pitch
        if gui_state['current_editing'] == ('track_pitch_' .. tidx) then
            tracks[tidx]['pitch_edit_state'] = textfield({10, y, 30, 20}, tracks[tidx]['pitch_edit_state']);
            if tracks[tidx]['pitch_edit_state'].cursoridx == nil then
                gui_state['current_editing'] = nil;
                local p = tonumber(tracks[tidx]['pitch_edit_state'].text);
                if (p == nil) or (p > 127) or (p < 1) then
                    p = tracks[tidx]['pitch'];
                elseif p ~= tracks[tidx]['pitch'] then
                    delete_notes_of_pitch(tracks[tidx]['pitch']);
                    tracks[tidx]['pitch'] = p;
                    tracks[tidx]['notes'] = collect_notes_for_pitch(p);
                    save_settings();
                end
            end
        else
            if label({10, y, 30, 20}, '#' .. tostring(tracks[tidx]['pitch'])) then
                gui_state['current_editing'] = ('track_pitch_' .. tidx);
                tracks[tidx]['pitch_edit_state'].text = tostring(tracks[tidx]['pitch']);
                tracks[tidx]['pitch_edit_state'].cursoridx = #tracks[tidx]['pitch_edit_state'].text + 1;
            end
        end

        -- Name
        if gui_state['current_editing'] == ('track_name_' .. tidx) then
            tracks[tidx]['name_edit_state'] = textfield({45, y, 75, 20}, tracks[tidx]['name_edit_state']);
            if tracks[tidx]['name_edit_state'].cursoridx == nil then
                gui_state['current_editing'] = nil;
                local s = tracks[tidx]['name_edit_state'].text;
                if (s == nil) or (#s == 0) then
                    s = tracks[tidx]['name'];
                else
                    tracks[tidx]['name'] = s;
                    save_settings();
                end
            end
        else
            if label({45, y, 75, 20}, tracks[tidx]['name']) then
                gui_state['current_editing'] = ('track_name_' .. tidx);
                tracks[tidx]['name_edit_state'].text = tracks[tidx]['name'];
                tracks[tidx]['name_edit_state'].cursoridx = #tracks[tidx]['name_edit_state'].text + 1;
            end
        end

        -- Beats
        local note_array = tracks[tidx]['notes'];
        for idx = 1, total_beats do
            local bs = (idx-1) // 4 % 4;
            if (bs == 1) or (bs == 3) then
                gui_state['skin']['fgcolor'] = {0.7, 0.7, 0.7, 1};
            else
                gui_state['skin']['fgcolor'] = {1, 1, 1, 1};
            end

            local was_checked = false;
            if note_array[idx] ~= false then
                was_checked = true;
            end
            local now_checked = checkbox({125 + (20 * (idx - 1)), y, 20, 20}, '', was_checked);
            if was_checked ~= now_checked then
                local mi_start_sec = reaper.GetMediaItemInfo_Value(active_mi, 'D_POSITION');
                if now_checked then
                    -- insert note at current beat
                    local startppq = reaper.MIDI_GetPPQPosFromProjTime(active_mt, mi_start_sec + ((idx - 1) * beat_sec));
                    local endppq = reaper.MIDI_GetPPQPosFromProjTime(active_mt, mi_start_sec + (idx * beat_sec));
                    reaper.MIDI_InsertNote(active_mt, false, false, startppq, endppq, 0, tracks[tidx]['pitch'], 100, false);
                    tracks[tidx]['notes'] = collect_notes_for_pitch(tracks[tidx]['pitch']);
                    note_array = tracks[tidx]['notes'];
                else
                    -- remove note at current beat
                    reaper.MIDI_DeleteNote(active_mt, find_note_index(tracks[tidx]['pitch'], mi_start_sec + ((idx - 1) * beat_sec)));
                    note_array[idx] = false;
                end
            end
        end

        gui_state['skin']['fgcolor'] = {1, 1, 1, 1};
        tidx = tidx + 1;
    end

    -- Buttons
    local y = gfx.h - 25;
    if button({10, y, 100, 20}, 'Add ...') then
        local tname = 'P '.. tostring(next_track_pitch);
        tracks[#tracks + 1] = build_track_data(tname, next_track_pitch);
        save_settings();

        next_track_pitch = next_track_pitch + 1;
        setprop('next_pitch', tostring(next_track_pitch));
    end

end

function toggle_play()
    local playing = false;
    if 0 == reaper.GetPlayState() then
        -- Start Play
        reaper.GetSetRepeat(1);   -- ensure repeat

        local mi_start_sec = reaper.GetMediaItemInfo_Value(active_mi, 'D_POSITION');
        local take_src = reaper.GetMediaItemTake_Source(active_mt);
        local src_len, isqn = reaper.GetMediaSourceLength(take_src);
        if isqn then  -- ensure convert to sec
            src_len = reaper.TimeMap_QNToTime_abs(0, src_len);
        end

        -- ensure loop time range
        reaper.GetSet_LoopTimeRange(true, true, mi_start_sec, mi_start_sec + src_len, true);

        reaper.OnPlayButton();
        playing = true;
    else
        -- Stop
        reaper.OnStopButton();
    end
    return playing;
end

-- The main loop of the plugin window.
-- It detects both mouse and keyboard input and calls 'do_gui' for
-- either repainting or event-dispatching.
function main_loop()
    if reaper.MIDIEditor_GetActive() == nil then
        return;
    end

    local bpm, bpi = reaper.GetProjectTimeSignature();
    if (active_bpm ~= bpm) or (active_bpi ~= bpi) then
        return;
    end

    -- Re-calculate total_beats per frame
    update_total_beat_count();

    -- handle key input
    local ch = gfx.getchar()
    if (ch == 27) or (ch == -1) then
        return;
    else
        if 0 ~= ch then
            log(tostring(ch) .. '\n');

            if 32 == ch then  -- space key
                toggle_play();
            else
                gui_state.event_type = 3;
                gui_state.key_code = ch;
                do_gui();
            end
        end
        reaper.defer(main_loop);
    end

    -- handle mouse input
    if (gfx.mouse_cap == 0) and (gui_state.mouse_down == true) then
        gui_state.mouse_down = false;
        log('MouseUp x:' .. gfx.mouse_x .. ', y:' .. gfx.mouse_y .. '\n');

        gui_state.event_type = 2;
        do_gui();
    elseif (gfx.mouse_cap & 1) ~= 0 then
        gui_state.mouse_down = true;

        gui_state.event_type = 1;
        do_gui();
    end

    gui_state.event_type = 0;
    do_gui();
    gfx.update();
end

-- Return the source length of current editing MIDI take. (in seconds.)
function get_take_source_length()
    local take_src = reaper.GetMediaItemTake_Source(active_mt);
    local src_len, isqn = reaper.GetMediaSourceLength(take_src);
    if isqn then  -- ensure convert to sec
        src_len = reaper.TimeMap_QNToTime_abs(0, src_len);
    end
    return src_len;
end

function update_total_beat_count()
    local src_len = get_take_source_length();
    total_beats = math.floor((src_len / beat_sec) + 0.5);
    total_beats = math.max(math.min(total_beats, 64), 0);
end

function delete_notes_of_pitch(pitch)

    todelete = {};

    -- select all notes for iteration
    reaper.MIDI_SelectAll(active_mt, true);

    local mi_start_sec = reaper.GetMediaItemInfo_Value(active_mi, 'D_POSITION');
    local mi_end_sec = mi_start_sec + get_take_source_length();

    -- iteration
    local note_idx = -2;
    while true do
        note_idx = reaper.MIDI_EnumSelNotes(active_mt, note_idx);
        if note_idx == -1 then  -- end of iteration
            break;
        end

        local retval, selected, muted, startppqpos, endppqpos, chan, npitch, vel = reaper.MIDI_GetNote(active_mt, note_idx);
        if npitch == pitch then
            local note_start_sec = reaper.MIDI_GetProjTimeFromPPQPos(active_mt, startppqpos);
            if (note_start_sec >= mi_start_sec) and (note_start_sec <= mi_end_sec) then
                todelete[#todelete + 1] = note_idx;
            end
        end
    end

    -- deselect all notes after done
    reaper.MIDI_SelectAll(active_mt, false);

    -- Delete notes (from back to head)
    if #todelete >= 1 then
        for i = #todelete, 1, -1 do
            reaper.MIDI_DeleteNote(active_mt, todelete[i]);
        end
    end
end

function find_note_index(pitch, timepos)
    -- select all notes for iteration
    reaper.MIDI_SelectAll(active_mt, true);

    local mi_start_sec = reaper.GetMediaItemInfo_Value(active_mi, 'D_POSITION');
    local mi_end_sec = mi_start_sec + get_take_source_length();

    -- iteration
    local note_idx = -2;
    while true do
        note_idx = reaper.MIDI_EnumSelNotes(active_mt, note_idx);
        if note_idx == -1 then  -- end of iteration
            break;
        end

        local retval, selected, muted, startppqpos, endppqpos, chan, npitch, vel = reaper.MIDI_GetNote(active_mt, note_idx);
        if npitch == pitch then
            local note_start_sec = reaper.MIDI_GetProjTimeFromPPQPos(active_mt, startppqpos);
            if (note_start_sec >= mi_start_sec) and (note_start_sec <= mi_end_sec) then
                if math.abs(note_start_sec - timepos) < 0.0625 then
                    reaper.MIDI_SelectAll(active_mt, false);
                    return note_idx;
                end
            end
        end
    end

    -- deselect all notes after done
    reaper.MIDI_SelectAll(active_mt, false);
end

-- Scan through all MIDI notes and collect those note with the specified pitch.
-- Return: An array (table) of booleans cooresponding to each beat slot. 
--         'true' if there is a beat, 'false' if there is not.
function collect_notes_for_pitch(pitch)
    local a = {};

    for idx = 1, 64 do
        a[idx] = false;
    end

    local mi_start_sec = reaper.GetMediaItemInfo_Value(active_mi, 'D_POSITION');
    local mi_end_sec = mi_start_sec + get_take_source_length();

    -- select all notes for iteration
    reaper.MIDI_SelectAll(active_mt, true);

    -- iteration
    local note_idx = -2;
    while true do
        note_idx = reaper.MIDI_EnumSelNotes(active_mt, note_idx);
        if note_idx == -1 then  -- end of iteration
            break;
        end

        local retval, selected, muted, startppqpos, endppqpos, chan, npitch, vel = reaper.MIDI_GetNote(active_mt, note_idx);
        if npitch == pitch then
            local note_start_sec = reaper.MIDI_GetProjTimeFromPPQPos(active_mt, startppqpos);
            if (note_start_sec >= mi_start_sec) and (note_start_sec <= mi_end_sec) then
                local i = math.floor((note_start_sec - mi_start_sec) / beat_sec) + 1;
                if i <= total_beats then
                    a[i] = true;
                end
            end
        end
    end

    -- deselect all notes after done
    reaper.MIDI_SelectAll(active_mt, false);

    return a;
end

function build_track_data(name, pitch)
    local ret = {};
    ret['name'] = name;
    ret['pitch'] = pitch;
    ret['notes'] = collect_notes_for_pitch(pitch);
    ret['pitch_edit_state'] = {};
    ret['name_edit_state'] = {};
    return ret;
end

function save_settings()
    local idx = 1;
    while true do
        if tracks[idx] == nil then
            break;
        end

        setprop('pitch' .. tostring(idx), tostring(tracks[idx]['pitch']));
        setprop('name' .. tostring(idx), tracks[idx]['name']);
        idx = idx + 1;
    end
end

-- load from persistent props
function load_settings()
    active_mt = reaper.MIDIEditor_GetTake(reaper.MIDIEditor_GetActive());
    active_mi = reaper.GetMediaItemTake_Item(active_mt);

    beat_sec = 60 / active_bpm / 4;
    update_total_beat_count();

    if nil == getprop('pitch'..1) then
        setprop('pitch'..1, '36');
        setprop('name'..1, 'Kick');
        setprop('pitch'..2, '38');
        setprop('name'..2, 'Snare');
        setprop('pitch'..3, '39');
        setprop('name'..3, 'Clap');
        setprop('pitch'..4, '42');
        setprop('name'..4, 'Hats');
        setprop('next_pitch', '43');
        log('Populate properties.');
    end

    local idx = 1;
    tracks = {};
    while true do
        local p = getprop('pitch'..tostring(idx));
        if nil == p then
            break;
        end

        local name = tostring(getprop('name'..tostring(idx)));
        tracks[#tracks + 1] = build_track_data(name, tonumber(p));
        idx = idx + 1;
    end

    next_track_pitch = tonumber(getprop('next_pitch'));
end

--[[ The entry point of plugin ]]

-- require an active MIDI editor to work
active_editor = reaper.MIDIEditor_GetActive();
-- bpm: how many beat per minute. bpi: how many beat per interval.
active_bpm, active_bpi = reaper.GetProjectTimeSignature();

if nil == active_editor then
    reaper.ShowMessageBox('No active MIDI editor.', 'Error', 0);
else
    gfx.init('Beat Donkey v0.1', 450, 300, 0, 100, 100);
    load_settings();
    reaper.defer(main_loop)
end
