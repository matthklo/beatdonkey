# Beat Donkey (Reaper Plugin)

**Beat Donkey** is a Reaper plugin writtern in Lua.

I develope it to help me composing game BGMs.

Beat Donkey was inspired by the beat maker of FL Studio and works pretty much in the similar way. Though the UI is way ugly than FL Studio.

# Install
1. Have a working Reaper DAW
2. Download the 'beatdonkey.lua' and store it somewhere in your computer.
3. Launch Reaper.
4. Press '?' in Reaper to bring on action list dialog.
5. Click the 'Load...' button at the bottom-right corner of the dialog.
6. Pick the 'beatdonkey.lua' you just downloaded.
7. A new entry named: 'Script: beatdonkey.lua' should appear in the action list for now on. Select it and click the 'Run' button to run it. Assign a key binding on it is recommended.

# Usage
1. Double-click on any MIDI item in your project, which will bring on a MIDI editor.
2. Run 'beatdonkey.lua' in the action list.
3. In the plugin dialog, you can add / remove a beat simply by ckecking / unchecking on those little squares.
4. By default there will be 4 beat tracks for the most common percussion sounds: Base(Kick) (36), Snare (38), Clap (39), and Close HiHats (42). Feel free to change their pitch / names or add more beat tracks to suit your needs. These beat track settings will be saved along with your project, and it is shared by all tracks. However the notes on each track are saved within the MIDI item.
5. Press space key to start / stop playing.
6. Please note that you still have to assign a proper VST instruments for that MIDI track. Beat Donkey does not produce any sounds, it just help you arraging the MIDI notes.